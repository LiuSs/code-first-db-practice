﻿namespace CodeFirstDb
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_book = new System.Windows.Forms.DataGridView();
            this.booksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource_BookstoreDb = new System.Windows.Forms.BindingSource(this.components);
            this.bookstoreDbDataSet = new CodeFirstDb.BookstoreDbDataSet();
            this.textBox_bookName = new System.Windows.Forms.TextBox();
            this.textBox_bookPublisher = new System.Windows.Forms.TextBox();
            this.button_Add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.booksTableAdapter = new CodeFirstDb.BookstoreDbDataSetTableAdapters.BooksTableAdapter();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_bookId = new System.Windows.Forms.TextBox();
            this.button_bookDelete = new System.Windows.Forms.Button();
            this.bookId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookPublisher = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_book)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.booksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_BookstoreDb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookstoreDbDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_book
            // 
            this.dataGridView_book.AllowUserToOrderColumns = true;
            this.dataGridView_book.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_book.AutoGenerateColumns = false;
            this.dataGridView_book.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_book.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView_book.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_book.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bookId,
            this.bookName,
            this.bookPublisher,
            this.bookTime});
            this.dataGridView_book.DataSource = this.booksBindingSource;
            this.dataGridView_book.Location = new System.Drawing.Point(12, 12);
            this.dataGridView_book.Name = "dataGridView_book";
            this.dataGridView_book.RowTemplate.Height = 24;
            this.dataGridView_book.Size = new System.Drawing.Size(490, 270);
            this.dataGridView_book.TabIndex = 0;
            this.dataGridView_book.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_book_CellContentClick);
            // 
            // booksBindingSource
            // 
            this.booksBindingSource.DataMember = "Books";
            this.booksBindingSource.DataSource = this.bindingSource_BookstoreDb;
            // 
            // bindingSource_BookstoreDb
            // 
            this.bindingSource_BookstoreDb.DataSource = this.bookstoreDbDataSet;
            this.bindingSource_BookstoreDb.Position = 0;
            this.bindingSource_BookstoreDb.CurrentChanged += new System.EventHandler(this.bindingSource_BookstoreDb_CurrentChanged);
            // 
            // bookstoreDbDataSet
            // 
            this.bookstoreDbDataSet.DataSetName = "BookstoreDbDataSet";
            this.bookstoreDbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBox_bookName
            // 
            this.textBox_bookName.Location = new System.Drawing.Point(12, 322);
            this.textBox_bookName.Name = "textBox_bookName";
            this.textBox_bookName.Size = new System.Drawing.Size(100, 22);
            this.textBox_bookName.TabIndex = 1;
            // 
            // textBox_bookPublisher
            // 
            this.textBox_bookPublisher.Location = new System.Drawing.Point(142, 322);
            this.textBox_bookPublisher.Name = "textBox_bookPublisher";
            this.textBox_bookPublisher.Size = new System.Drawing.Size(100, 22);
            this.textBox_bookPublisher.TabIndex = 2;
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(277, 320);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(75, 23);
            this.button_Add.TabIndex = 3;
            this.button_Add.Text = "新增";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 304);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "書本名稱";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(142, 303);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "出版商";
            // 
            // booksTableAdapter
            // 
            this.booksTableAdapter.ClearBeforeFill = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 362);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "編號";
            // 
            // textBox_bookId
            // 
            this.textBox_bookId.Location = new System.Drawing.Point(17, 391);
            this.textBox_bookId.Name = "textBox_bookId";
            this.textBox_bookId.Size = new System.Drawing.Size(100, 22);
            this.textBox_bookId.TabIndex = 7;
            // 
            // button_bookDelete
            // 
            this.button_bookDelete.Location = new System.Drawing.Point(144, 391);
            this.button_bookDelete.Name = "button_bookDelete";
            this.button_bookDelete.Size = new System.Drawing.Size(75, 23);
            this.button_bookDelete.TabIndex = 8;
            this.button_bookDelete.Text = "刪除";
            this.button_bookDelete.UseVisualStyleBackColor = true;
            this.button_bookDelete.Click += new System.EventHandler(this.button_bookDelete_Click);
            // 
            // bookId
            // 
            this.bookId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.bookId.DataPropertyName = "bookId";
            this.bookId.Frozen = true;
            this.bookId.HeaderText = "書籍編號";
            this.bookId.Name = "bookId";
            this.bookId.ReadOnly = true;
            this.bookId.Width = 78;
            // 
            // bookName
            // 
            this.bookName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.bookName.DataPropertyName = "bookName";
            this.bookName.Frozen = true;
            this.bookName.HeaderText = "書籍名稱";
            this.bookName.Name = "bookName";
            this.bookName.Width = 78;
            // 
            // bookPublisher
            // 
            this.bookPublisher.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.bookPublisher.DataPropertyName = "bookPublisher";
            this.bookPublisher.Frozen = true;
            this.bookPublisher.HeaderText = "出版商";
            this.bookPublisher.Name = "bookPublisher";
            this.bookPublisher.Width = 66;
            // 
            // bookTime
            // 
            this.bookTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.bookTime.DataPropertyName = "bookAddTime";
            this.bookTime.HeaderText = "上架時間";
            this.bookTime.Name = "bookTime";
            this.bookTime.Width = 78;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 438);
            this.Controls.Add(this.button_bookDelete);
            this.Controls.Add(this.textBox_bookId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.textBox_bookPublisher);
            this.Controls.Add(this.textBox_bookName);
            this.Controls.Add(this.dataGridView_book);
            this.Name = "Form1";
            this.Text = "實體資料庫測試";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_book)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.booksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_BookstoreDb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookstoreDbDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_book;
        private System.Windows.Forms.TextBox textBox_bookName;
        private System.Windows.Forms.TextBox textBox_bookPublisher;
        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource bindingSource_BookstoreDb;
        private BookstoreDbDataSet bookstoreDbDataSet;
        private System.Windows.Forms.BindingSource booksBindingSource;
        private BookstoreDbDataSetTableAdapters.BooksTableAdapter booksTableAdapter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_bookId;
        private System.Windows.Forms.Button button_bookDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookName;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookPublisher;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookTime;
    }
}

