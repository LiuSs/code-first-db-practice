﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using CodeFirstDb.model;

namespace CodeFirstDb
{
    public partial class Form1 : Form
    {
        BookstoreDb db = new BookstoreDb();

        public Form1()
        {
            InitializeComponent();

        }

        private void dataGridView_book_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button_Add_Click(object sender, EventArgs e)
        {            
            Book aBook = new Book();

            aBook.bookName = textBox_bookName.Text;
            aBook.bookPublisher = textBox_bookPublisher.Text;
            aBook.bookAddTime = DateTime.Now;

            db.Book.Add(aBook);
            db.SaveChanges();

            this.booksTableAdapter.Fill(this.bookstoreDbDataSet.Books);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 這行程式碼會將資料載入 'bookstoreDbDataSet.Books' 資料表。您可以視需要進行移動或移除。
            this.booksTableAdapter.Fill(this.bookstoreDbDataSet.Books);

        }

        private void button_bookDelete_Click(object sender, EventArgs e)
        {
            Book aBook = db.Book.Find(Convert.ToInt32(textBox_bookId.Text));           
            db.Book.Remove(aBook);
            db.SaveChanges();
            this.booksTableAdapter.Fill(this.bookstoreDbDataSet.Books);
        }

        private void bindingSource_BookstoreDb_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
