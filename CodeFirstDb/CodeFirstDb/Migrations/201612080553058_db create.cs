namespace CodeFirstDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbcreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        bookId = c.Int(nullable: false, identity: true),
                        bookName = c.String(),
                        bookPublisher = c.String(),
                    })
                .PrimaryKey(t => t.bookId);
            
            CreateTable(
                "dbo.Readers",
                c => new
                    {
                        readerId = c.Int(nullable: false, identity: true),
                        readerBook = c.String(),
                        readerFeedback = c.String(),
                    })
                .PrimaryKey(t => t.readerId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Readers");
            DropTable("dbo.Books");
        }
    }
}
