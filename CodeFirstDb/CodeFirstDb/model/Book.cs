﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDb.model
{
    class Book
    {
        public int bookId { get; set; }
        public string bookName { get; set; }
        public string bookPublisher { get; set; }
        public DateTime bookAddTime { get; set; }
    }
}
