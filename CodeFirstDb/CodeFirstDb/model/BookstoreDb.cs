﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodeFirstDb.model
{
    class BookstoreDb:DbContext
    {
        //public BookstoreDb() : base("BookstoreDb") { } //直接在SQLexpress建立
        public BookstoreDb() : base("name=BookstoreDb") { } // 從config檔案指定實體資料庫

        //引入資料表(data set)
        public DbSet<Book> Book { get; set; }
        public DbSet<Reader> Reader { get; set; }
    }
}
