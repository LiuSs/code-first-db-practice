﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDb.model
{
    class Reader
    {
        public int readerId { get; set; }
        public string readerBook { get; set; }
        public string readerFeedback { get; set; }
    }
}
